function unitarny = czy_unitarny (v)

# V jest unitarny, kiedy VxV* = m. jednostkowa.

    unitarny = false;

    printf("\nCzy V jest unitarny?\n")

    printf( "##   V\n")
    v
    printf( "##   V*\n")
    v'
    printf( "##   V^-1\n" )
    v^-1

    if ( v*v' == eye(size(v)) )
        unitarny = true;
        printf("V jest unitarny, poniewaz V*=V^-1 => VxV*=m. jedn.\n")
    else
        unitarny = false;
        printf("V NIE jest unitarny, poniewaz V* != V^-1 => VxV* != m. jedn.\n")
    endif

    if ( (v' == v^-1) != unitarny )
        printf("ERROR: Cos jest BARDZO zle w tescie na unitarny")
    endif

endfunction
