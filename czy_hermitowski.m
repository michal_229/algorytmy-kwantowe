function hermitowski = czy_hermitowski (v)

# V jest hermitowski, kiedy V = V*.

    hermitowski = false;

    printf("\nCzy V jest hermitowski?\n")

    printf( "##   V\n")
    v
    printf( "##   V*\n")
    v'

    if ( v == v' )
        hermitowski = true;
        printf("V jest hermitowski, poniewaz V = V*.\n")
    else
        hermitowski = false;
        printf("V NIE jest hermitowski, poniewaz V != V*.\n")
    endif

    if (ishermitian(v) != hermitowski)
        printf("ERROR: Cos jest BARDZO zle w tescie na hermitowski")
    endif

endfunction
