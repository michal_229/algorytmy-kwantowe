init


a2=(kron(H2,eye(2)))*CNOT
b2=CH*kron(Z,N)*CNOT



psi=[2/sqrt(5);1/sqrt(5)]
fi=[1;3i]/sqrt(10)

psifi=[psi ;fi]



a2_psifi = a2*psifi


b2_psifi = b2*psifi



x = [2i/3 ; 0 ; 1/3 ; 2/3]
norm_x = norm(x)
mx = x*x'
if mx^-1 == mx' printf ("unitarny...\n") end
Mx = mx*mx'

y = [ 1/4+3i/4;1/2;0;1/4-i/4 ]
norm_y = norm(y)
my = y*y'
if my^-1 == my' printf ("unitarny...\n") end
My = my*my'

z = [3/5;-1/5+2i/5;3i/5;1/5]
norm_z = norm(z)
mz = z*z'
if mz^-1 == mz' printf ("unitarny...\n") end
Mz = mz*mz'
