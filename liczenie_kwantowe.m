init

printf("\nTemat 1: Przestrzenie Hilberta. Operatory hermitowskie i unitarne, reprezentacje macierzowe operatorow.\n")
printf("\n1. Oblicz następujące iloczyny skalarne (w przestrzeni Hilberta C^n):\n")
a = [1 i -1]; b = [i 2 (3-i)];
printf("\na) (%s|%s)\n", mat2str(a), mat2str(b))
dot(a, b)

a = [2*i -2*i (2+i)]; b = [0 -1 -i];
printf("\nb) (%s|%s)\n", mat2str(a), mat2str(b))
dot(a, b)

a = [-3+i 1-2i]; b = [-1-i 2];
printf("\nc) (%s|%s)\n", mat2str(a), mat2str(b))
dot(a, b)

a = [4+2i 2-6i]; b = [-1+3i 2+i];
printf("\nd) (%s|%s)\n", mat2str(a), mat2str(b))
dot(a, b)

printf("\n2. Oblicz iloczyn tensorowy macierzy A (+) B:\n")
a = [3 -1 2]; b = [1 -2].';
printf("\na) A = %s,  B = %s\n", mat2str(a), mat2str(b))
kron(a, b)

a = [5 0 ; 3 2]; b = [1 -2 2];
printf("\nb) A = %s,  B = %s\n", mat2str(a), mat2str(b))
kron(a, b)

a = [-3 1 -1 ; 1 3 -2]; b = [-7; 3; 5];
printf("\nc) A = %s,  B = %s\n", mat2str(a), mat2str(b))
kron(a, b)

a = [0 1 2 ; -2 5 3]; b = [2 4 ; -1 3];
printf("\nd) A = %s,  B = %s\n", mat2str(a), mat2str(b))
kron(a, b)

printf("\n3. Obliczyc operator sprzezony A* operatora A zapisanego w reprezentacji macierzowej:\n")
printf("\na) ")
A = [i 1; 2 -i]
A'

printf("\nb) ")
A = [0 -i; i 0]
A'

printf("\nc) ")
A = [1 0 -1; i -i 0; i 0 1]
A'

printf("\n4. Sprawdzic, czy operator A jest hermitowski i czy jest unitarny:\n")
printf("\na) ")
A = [0 -i; i 0]
if A==A' printf("A jest hermitowski, poniewaz A = A*.\n") else printf("A nie jest hermitowski, poniewaz A!=A*.\n") end
if A*A' == eye(size(A)) printf("A jest unitarny, poniewaz AxA* == m. jednostkowa.\n") else printf("A nie jest unitarny, poniewaz AxA* != m. jednostkowa.\n") end

printf("\nb) ")
A = [1 0; 0 2]
if A==A' printf("A jest hermitowski, poniewaz A = A*.\n") else printf("A nie jest hermitowski, poniewaz A!=A*.\n") end
if A*A' == eye(size(A)) printf("A jest unitarny, poniewaz AxA* == m. jednostkowa.\n") else printf("A nie jest unitarny, poniewaz AxA* != m. jednostkowa.\n") end

printf("\nc) ")
A = [i 0; 0 -i]
if A==A' printf("A jest hermitowski, poniewaz A = A*.\n") else printf("A nie jest hermitowski, poniewaz A!=A*.\n") end
if A*A' == eye(size(A)) printf("A jest unitarny, poniewaz AxA* == m. jednostkowa.\n") else printf("A nie jest unitarny, poniewaz AxA* != m. jednostkowa.\n") end

printf("\nd) ")
A = [0 -i 1 ; i 1 i ; 1 -i 0]
if A==A' printf("A jest hermitowski, poniewaz A = A*.\n") else printf("A nie jest hermitowski, poniewaz A!=A*.\n") end
if A*A' == eye(size(A)) printf("A jest unitarny, poniewaz AxA* == m. jednostkowa.\n") else printf("A nie jest unitarny, poniewaz AxA* != m. jednostkowa.\n") end

printf("\n5. Znalezc baze przestrzeni H = H'(x)H\" (B' - baza przestrzeni H', B\" - baza przestrzeni H\") oraz policzyc wspolrzedne wektora z = x (x) y (x w H', y w H\", z w H)):\n")
B_ = [1 0 ; 0 1];
B__ = [1 0 ; 0 1];
x_ = [2 -1];
y__ = [1 2];
printf("\na) B' = %s, \nB\" = %s, \nx = %s, \ny = %s.\n", mat2str(B_), mat2str(B__), mat2str(x_), mat2str(y__))
H = kron(B_, B__)
x = x_ * B_;
y = y__ * B__;
z = kron(x, y)

B_ = [1 1 ; 1 -1]/sqrt(2);
B__ = [1 0 ; 0 1];
x_ = [1 3 ]/sqrt(2);
y__ = [1 2];
printf("\nb) B' = %s, \nB\" = %s, \nx = %s, \ny = %s.\n", mat2str(B_), mat2str(B__), mat2str(x_), mat2str(y__))
B = kron(B_, B__)
x = x_ * B_;
y = y__ * B__;
z = kron(x, y)*B

B_ = [1 1 ; 1 -1]/sqrt(2);
B__ = [1 0 ; 0 1];
x_ = [1 3 ]/sqrt(2);
y__ = [3 -1]/sqrt(2);
printf("\nc) B' = %s, \nB\" = %s, \nx = %s, \ny = %s.\n", mat2str(B_), mat2str(B__), mat2str(x_), mat2str(y__))
B = kron(B_, B__)
x = x_ * B_;
y = y__ * B__;
z = kron(x, y)*B


printf("\n6. Sprawdzic, czy element z przestrzeni H = H'(x)H\" jest splatany\n")
B_ = [1 0 ; 0 1];
B__ = [1 0 ; 0 1];
printf("B' = %s - baza przestrzeni H',\nB\" = %s - baza przestrzeni H\",\nB = B'(x)B\".\n", mat2str(B_), mat2str(B__))

printf("dokonczyc...")

z = [1 0 1 0];
printf("\na) z = %s\n", mat2str(z))



z = [0 2 1 0];
printf("\nb) z = %s\n", mat2str(z))






printf("\nTemat 2: Opis ukladu kwantowego i jego stanow, kubit, rejestr kwantowy. Opis i wlasnosci pomiaru.\n")
printf("\n1. Znalezc baze przestrzeni rejestru kwantowego 3-kubitowego oraz znalezc stan tego rejestru dla nastepujacych kubitow:\n")

q1 = [0.6 ; 0.8]
q2 = [1 ; 0]
q3 = [i ; 1]/sqrt(2)

H2 = [1 0 ; 0 1 ]
B3 = kron(H2, H2, H2)
Q = kron(q1, q2, q3)


printf("\n2. Dla podanych wektorow |psi>, |fi>, |omega> wykonac nastepujace operacje:\n")
psi = [1 ; 0 ; -1]
fi =  [0 ; -i ; i]
omega = [0.5 ; 0.5i ; -0.5i]



printf("\na) |psi> + |fi>\n")
psi + fi

printf("\nb) <psi| + <fi|\n")
psi' + fi'

printf("\nc) <fi|fi>\n")
fi' * fi

printf("\nd) <psi|omega>\n")
psi' * omega

printf("\ne) |fi><psi|\n")
fi * psi'

printf("\ne) <omega|fi><psi|\n")
 omega' * fi * psi'

printf("\n3. Obliczyc macierz rzutowania na podprzestrzen generowana przez wektor x:\n")
printf("\na)\n")
x = [0.4; 0.2; 0.4; 0.8]
printf("\nMx = |x><x| = \n")
x*x'

printf("\nb)\n")
x = [1/sqrt(2); 1/2; 0; i/2]
printf("\nMx = |x><x| = \n")
x*x'

printf("\nc)\n")
x = [1/2; i/2; -1/2; -i/2]
printf("\nMx = |x><x| = \n")
x*x'

printf("\n4. Czy podane wektory moga reprezentowac stan kwantowy?\n")
printf("\na) ")
x = [0.2; 0.4; 0.6; 0.8]
if norm(x) == 1.0 printf("Moze, poniewaz ||x|| = 1")
else printf("Nie moze, poniewaz ||x|| != 1")
end

printf("\nb) ")
x = [1/2; i/2; -1/2; -i/2]
if norm(x) == 1.0 printf("Moze, poniewaz ||x|| = 1")
else printf("Nie moze, poniewaz ||x|| != 1")
end


printf("\n5. Sprawdzic, czy podane stany kwantowe sa czyste, czy mieszane:\n")
printf("\na) ")
M = [
    1/2 1/(2*sqrt(2)) 0 1/(2*sqrt(2)) ;
    1/(2*sqrt(2)) 1/4 0 1/4 ;
    0 0 0 0 ;
    1/(2*sqrt(2)) 1/4 0 1/4
]
if trace(M^2) == 1.0 printf("Stan czysty, poniewaz Tr(M^2) = 1")
else printf("Stan mieszany, poniewaz Tr(M^2) = %f < 1", trace(M^2)) end

printf("\nb) ")
M = [
    1/4 1/(4*sqrt(2)) 0 1/(4*sqrt(2)) ;
    1/(4*sqrt(2)) 3/8 0 3/8 ;
    0 0 0 0 ;
    1/(4*sqrt(2)) 3/8 0 3/8
]
if trace(M^2) == 1.0 printf("Stan czysty, poniewaz Tr(M^2) = 1")
else printf("Stan mieszany, poniewaz Tr(M^2) = %.3f < 1", trace(M^2)) end




printf("\nTemat 3: Przetwarzanie informacji kwantowej: bramki kwantowe, obwody kwantowe, kopiowanie stanów.\n")
printf("\n1. Obliczyc wynik dzialania ponizszych bramek na kubity |psi>, |fi>, |omega> (sprawdzic takze, czy kubity sa stanami kwantowymi):\n")

fi = [1/sqrt(2) ; -1/sqrt(2)]
psi = [ i ; 0 ]
omega = [sqrt(3)/2 ; i/2]

NOT_fi = NOT*fi

NOT_psi = NOT*psi

NOT_omega = NOT*omega

H_fi = H2*fi

H_psi = H2*psi

H_omega = H2*omega

T_fi = T*fi

T_psi = T*psi

T_omega = T*omega
