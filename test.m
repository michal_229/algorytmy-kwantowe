init


a = [ 0 -i ; i 0];
b = [ 1 0 ; 0 2 ];
c = [ i 0 ; 0 -i ];
d = [ 0 -i 1 ; i 1 i ; 1 -i 0 ];

x = [2i/3 ; 0 ; 1/3 ; 2/3];
y = [ 1/4+3i/4;1/2;0;1/4-i/4 ];
z = [3/5;-1/5+2i/5;3i/5;1/5];

assert(czy_hermitowski(a), true)
assert(czy_unitarny(a), true)

assert(czy_hermitowski(b), true)
assert(czy_unitarny(b), false)

assert(czy_hermitowski(c), false)
assert(czy_unitarny(c), true)

assert(czy_hermitowski(d), true)
assert(czy_unitarny(d), false)

assert(czy_hermitowski(x*x'), true)
assert(czy_unitarny(x*x'), false)
assert(czy_hermitowski(y*y'), true)
assert(czy_unitarny(y*y'), false)
assert(czy_hermitowski(z*z'), true)
assert(czy_unitarny(z*z'), false)
